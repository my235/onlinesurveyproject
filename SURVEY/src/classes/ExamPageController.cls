public class ExamPageController {
    
    private static final String TRUE_FALSE_TYPE = 'True/False';
    private static final String MULTIPLE_CHOICE_TYPE = 'Multiple Choice';
    private static final String RADIO_TYPE = 'Radio';
    //private static final String TEXT_TYPE = 'Short Answer';
    private static final String MULTIPLE_CHOICE_SEPARATOR = '|';

    public List<Question__c> questions{get;set;}
    public Id surveyId{get;set;} // used to get survey
    public Id contactId{get;set;} // used to get contact
    public Id surveyResponseId {get;set;} //used to get survey response id
    public SurveyResponse__c surveyResponse {get;set;}
    public Survey__c survey{get;set;}
    
    
    public Map<String,String> responseByQuestionId{get;set;} // stores answers to questions. short answer, radio, true/false
    public Map<String,List<String>> selectedOptionsByQuestionId{get;set;} // stores answers to multiple choice questions
    public Map<String,List<SelectOption>> availableOptionsByQuestionId{get;set;} // used to fill available options on the form
    public Integer NumQuestions{get;set;}
    
    
    public ExamPageController(){
    	
    }
    
    
    public PageReference start(){
    	return Page.ExamPage;
    }
    
    public PageReference submit(){
    	return Page.ExamScorePage;
    }
    
    
}