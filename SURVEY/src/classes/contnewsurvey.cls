public class contnewsurvey {
    public string strsurveyname{get;set;}
    public string strsurveyrecordtypeid{get;set;}
   public list<SelectOption> surveytypes{get;set;}
    public decimal numberofminutes{get;set;}
    public decimal numberofquestions{get;set;}
    public boolean randomized{get;set;}
    public boolean showscore{get;set;}
    public boolean showonequestion{get;set;}
    public boolean pageblock1{get;set;}
    public boolean pageblock2{get;set;}
    public boolean pageblock3{get;set;}
    public boolean pageblock4{get;set;}
    public boolean pageblock5{get;set;}
    public boolean pageblock6{get;set;}
    public string stroutputid{get;set;}
    public list<question__c> questions{get;set;}
    public list<selectoption> questiontypes{get;set;}
    public string questiontypeid{get;set;}
    public list<recordtype> questionrecordtypes{get;set;}
    
//012f4000000gjuqAAA is id for exam
//012f4000000gjvAAAQ is id for survey


public contnewsurvey (){
list<recordtype> templst = new list<recordtype>();
pageblock1= true;
pageblock2= false;
pageblock3= false;
pageblock4= false;
pageblock5= false;
pageblock6= false;
questiontypes = new list<selectoption>();



    surveytypes = new list<SelectOption>();
    if(stroutputid != null){
    questiontypes();
    }
    
    templst = [select name, id, SobjectType from recordtype where SobjectType = 'Survey__c'];
    for(recordtype r:templst){

        surveytypes.add(new SelectOption(r.Id, r.Name));
    }
}
    public void createsurvey(){
        survey__c s = new survey__c();
        s.Name = strsurveyname;
        s.recordtypeid = strsurveyrecordtypeid;
        if(strsurveyrecordtypeid == '012f4000000gjuqAAA'){
            s.NumberofMinutes__c = numberofminutes;
            s.NumberofQuestions__c = numberofquestions;
            s.Randomized__c = randomized;
            s.ShowScore__c = showscore;
            s.ShowOneQuestion__c = showonequestion;
            pageblock3=false;
        }
        insert s;
        stroutputid = s.id;
        pageblock2=true;
        
        if(strsurveyrecordtypeid == '012f4000000gjuqAAA'){
        
        questionrecordtypes = [select name, id, SobjectType from recordtype where SobjectType = 'Question__c' and name != 'Short Answer'];
for(recordtype r:questionrecordtypes){
questiontypes.add(new selectoption(r.id, r.name));
}
}
else {
questionrecordtypes = [select name, id , SobjectType from recordtype where SobjectType  = 'Question__c'];
for(recordtype r: questionrecordtypes){
questiontypes.add(new selectoption(r.id,r.name));
}
}
}
    
    
    public void afterchosensurveytype(){
    pageblock1=false;
    if(strsurveyrecordtypeid == '012f4000000gjuqAAA'){
    pageblock3=true;
    } else {
        createsurvey();
    
    }

}
public void questiontypes(){
Survey__c templst = [SELECT id, (SELECT Question__c FROM SurveyJunction__r) FROM Survey__c where id =: stroutputid];
list<string> questionIds = new List<String>();
for(SurveyJoinQuestion__c temp : templst.SurveyJunction__r){
    questionIds.add(temp.Question__c);
}
system.debug(questionids);
questions=[select Question_Text__c, id from question__C where id in: questionIds ];
}

public void makenewquestion(){
pageblock4=true;
}
public void linkquestion(){
pageblock5=true;
}
}