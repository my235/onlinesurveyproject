/**
 * Created by Tim on 8/20/2017.
 */
public class ControllerSurveySummary {
    public string surveyid{get;set;}
    public string strsurveyresults{get;set;}

    public list<selectoption> getlstsurveyoption(){
        list<survey__c> surveyoptions=[select id, name from Survey__c];
        list<selectoption> surveys=new list<SelectOption>();
        for(Survey__c temp : surveyoptions)
            {
                surveys.add(new SelectOption(temp.Id, temp.name));
            }
        return surveys;
    }

    public string getSurveyResults(){

        list<surveyResponse__c> lstSurveyResponse = [select id from surveyResponse__c where id =: surveyId];

        list<answers__c> lstAnswers = [select question__r.Question_Text__c, question__r.RecordTypeId, textFieldAnswer__c, surveyResponse__c from answers__c where surveyResponse__c =: lstSurveyResponse.get(0).id AND question__r.RecordTypeId != '012f4000000gTre'];

        map<string,list<string>> mapQuestionsAnswers = new map<string,list<string>>();
        set<string> stQuestionText = new set<string>();
        list<string> lstTempList = new list<string>();
        for(answers__c a: lstAnswers){
            if(stQuestionText.contains(a.question__r.question_text__c) == false){
                stQuestionText.add(a.question__r.question_text__c);
            }
        }
        for(string s:stQuestionText){
            lsttemplist.clear();
            for(answers__c a:lstAnswers){
                if(a.question__r.question_text__c == s){
                    lstTempList.add(a.textFieldAnswer__c);
                }
                mapQuestionsAnswers.put(s,lstTempList);
            }
        }
        strSurveyResults = JSON.serialize(mapQuestionsAnswers);
        return strSurveyResults;

    }
}