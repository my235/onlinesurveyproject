<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <tab>standard-AppLauncher</tab>
    <tab>Answers__c</tab>
    <tab>Survey__c</tab>
    <tab>SurveyJoinQuestion__c</tab>
    <tab>SurveyResponse__c</tab>
</CustomApplication>
