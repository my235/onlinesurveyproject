//Controller
public with sharing class DynamicQuestionCreate {

    public String rectypes { get; set; }
    public Question__c newQuestionText {get;set;}
    public SurveyJoinQuestion__c newSurveyJoinQuestion {get;set;}
    public list<Question__c> lstQuestions {get;set;}
    public list<Survey__c> lstSurveys {get;set;}
    public String RecordType { get; set; }
    public list<Question__c> lstRecordTypes;
    
    public list<SelectOption> surveyOptions {get;set;}
    public Id selectedObject {get;set;}
    
    //Constructor
    public DynamicQuestionCreate() {
        
        newQuestionText = new Question__c();
        lstQuestions = [select Question_Text__c from Question__c];
        lstSurveys = [select Name,SurveyName__c from Survey__c];
        surveyOptions = new List<SelectOption>();
        newSurveyJoinQuestion = new SurveyJoinQuestion__c();
    }
    
    public list<SelectOption> getSurveys() {
        list<Survey__c> lstSurveys = [select Id,Name, SurveyName__c from Survey__c];
        surveyOptions.add(new SelectOption('--None--','-'));
        for(Survey__c survey:lstSurveys){
            
            surveyOptions.add(new SelectOption(survey.Id, survey.Name));
        }
        return surveyOptions;
    }

    
    public list<SelectOption> getRecType() {
        list<SelectOption> rectypes = new list<SelectOption>();
        string obj = 'Question__c';
        rectypes.add(new selectOption('','- None -'));
        
        Id recTypeMC = Schema.SObjectType.Question__c.getRecordTypeInfosByName().get('Multiple Choice').getRecordTypeId();
        Id recTypeRadio = Schema.SObjectType.Question__c.getRecordTypeInfosByName().get('Radio').getRecordTypeId();
        Id recTypeShortAnswer = Schema.SObjectType.Question__c.getRecordTypeInfosByName().get('Short Answer').getRecordTypeId();
        Id recTypeTF = Schema.SObjectType.Question__c.getRecordTypeInfosByName().get('True/False').getRecordTypeId();
		
        rectypes.add(new selectOption(rectypeMC,'Multiple Choice'));
        rectypes.add(new selectOption(recTypeRadio,'Radio'));
        rectypes.add(new selectOption(recTypeShortAnswer, 'Short Answer'));
        rectypes.add(new selectOption(recTypeTF, 'True/False'));
        
        return rectypes;
    }
        
    
    
    public pageReference createQuestion() {
        try {
            //(Question__c)paramMap.get()
            newSurveyJoinQuestion.Question__c = newQuestionText.Question_Text__c;
            //newSurveyJoinQuestion.Survey__c = 
            insert newQuestionText;
            PageReference newQuestionPage = new PageReference('/' + lstSurveys);
        } catch(Exception e) {
        }
        return null;
    }
}