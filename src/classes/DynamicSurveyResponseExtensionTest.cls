/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class DynamicSurveyResponseExtensionTest {

    /*
     * Creates survey objects, survey/question junction objects, and question objects for test
     * 
     * If setup fails, that means there is an issue with the data model.
     * 
     */ 
    @TestSetup static void setup(){
        
        
        List<Question__c> lstQues = new List<Question__c>();
        List<SurveyJoinQuestion__c> listJunc = new List<SurveyJoinQuestion__c>();
        
       	/*
       	 * Creates two test contacts: one to be emailed and one to be deleted 
       	 */
        Contact testUser = new Contact(FirstName='test',LastName='user',email='testuser@test.com');
        insert testUser;
		
        Contact deleteUser = new Contact(FirstName='delete',LastName='me',email='deleteme@test.com');
		insert deleteUser;        

		/*
		 * Creates test survey
		 */ 
        
		Survey__c surv = new Survey__c(name='test survey' ,RecordTypeId=Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get('Survey').getRecordTypeId());
        insert surv;
        
        
        
        /*
         * Creates a Survey response object
         */
        
        SurveyResponse__c resp = new SurveyResponse__c(Survey__c = surv.id, Contact__c = testUser.id);
        insert resp;
        
        /*
         * Create four questions true/false,radio button, open ended, and multiple choice
         */
        
        //true or false
        Question__c testQuestion = new Question__c(
    		RecordTypeId=Schema.SObjectType.Question__c.getRecordTypeInfosByName().get('True/False').getRecordTypeId(),
    		Questionfor__c = 'Survey',
            Question_Text__c='True or false question');
        lstQues.add(testQuestion);
        
        //radio button
        testQuestion = new Question__c(
            RecordTypeId=Schema.SObjectType.Question__c.getRecordTypeInfosByName().get('Radio').getRecordTypeId(),
            Questionfor__c = 'Survey',
            Question_Text__c='Radio button question',
            Choice_A__c = 'Choice A',
            Choice_B__c = 'Choice B',
            Choice_C__c = 'Choice C',
            Choice_D__c = 'Choice D',
            Choice_E__c = 'Choice E'); 
		lstQues.add(testQuestion);
        
        //open ended
        testQuestion = new Question__c(
            RecordTypeId=Schema.SObjectType.Question__c.getRecordTypeInfosByName().get('Short Answer').getRecordTypeId(),
            Questionfor__c = 'Survey',
            Question_Text__c='Short answer question');
		lstQues.add(testQuestion);
        
        //multiple choice
        testQuestion = new Question__c(
            RecordTypeId=Schema.SObjectType.Question__c.getRecordTypeInfosByName().get('Multiple Choice').getRecordTypeId(),
            Questionfor__c = 'Survey',
            Question_Text__c='Multiple Choice question',
            Choice_A__c = 'Choice A',
            Choice_B__c = 'Choice B',
            Choice_C__c = 'Choice C',
            Choice_D__c = 'Choice D',
            Choice_E__c = 'Choice E');
    	lstQues.add(testQuestion);
        
        insert lstQues;
        
        /*
         * Create junction object for survey and question
         */ 
        for(integer i = 0;i < lstQues.size();i++){
            listJunc.add(new SurveyJoinQuestion__c(Survey__c=surv.id,Question__c=lstQues[i].id));
        }
        
        insert listJunc;
         
    }
	
	

    static testMethod void testSurveyResponse() {
        //creates a new page reference that references the DynamicSurveyForm visualforce page
        PageReference pref = Page.DynamicSurveyResponse;
        
        /*
         * the Id's of the survey and contact are used to populate the values in the URL
         */
        Survey__c surv = [Select Id,Name from Survey__c limit 1];
        Contact con = [Select id,name,email from Contact where FirstName='test' AND LastName='user' limit 1];
		SurveyResponse__c resp = [select id,name,Contact__c,Survey__c from SurveyResponse__c where Contact__c =: con.id AND Survey__c =: surv.id limit 1];
        pref.getParameters().put('surveyresponseid',resp.id);
        Test.setCurrentPage(pref);
    }
    
    /**
     * Not sure if this is needed yet
     
    static testMethod void testExamResponse(){
    	
    }
    */
}